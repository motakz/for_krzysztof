require 'spec_helper'


#TODO -change valuse which are assigne in spec to parameters

shared_examples "Create lead and check it" do
  it "open lead form" do
    on(LeadsPage).add_new_lead_element.when_visible(10).click
	#I've puted Time.now value to create different user name each time
    on(LeadsPage).lead_last_name_element.when_visible(10).value = Time.now
  end
  
  it "save lead" do
  on(LeadsPage).save_lead
  
  end
  
  it "add a note" do
  #TODO - add wait / method (?)
  on(LeadDetailPage).add_note_element.when_visible(10).value = "some text"
  on(LeadDetailPage).save_note
  end
  
  it "checks if lead is visible on tasks list" do
  #TODO - create correct assertion
   note = on(LeadDetailPage).note_content
   expect(note).to eq("some text")
  end

end
describe "Leads" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end

  after(:all) do
    on(LeadsPage).close_modal
  end

    describe "Create new lead" do
    #let(:related_name) { nil }
    include_examples "Create lead and check it"
  end

end