class LeadDetailPage < AbstractPage
  include PageObject
  include LeadModal
  include RelatedToPicker
  
  textarea(:add_note, :class => "span12")
  button(:save_note, :class => "btn btn-inverse hide")
  #think that locator is correct, but must catch ajax or make some wait
  div(:note_content, :xpath => "//p[@class = 'activity-content note-content']/child::*")
  
  
  
  
end